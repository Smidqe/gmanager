package application.extensions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/*
	This is a compilation of string related functions that don't exist or exists in different format

 */
public class strings {
	//checks if any of the values in list are found in the string, depending on count of those found will return true/false (it has to match or exceed)
	public static boolean contains(List<String> values, String string, int count)
	{
		int matches = 0;
		
		for (int i = 0; i < values.size(); i++)
			matches += string.contains(values.get(i)) ? 1 : 0;
		
		return (count != 0) ? matches >= count : matches > 0;
	}
	
	public static boolean contains(List<String> values, String string, boolean all)
	{
		return contains(values, string, all ? values.size() : 0);
	}
	
	public static boolean isArray(String string)
	{
		return contains(Arrays.asList("{", "}", "[", "]"), string, false);
	}
	
	public static List<String> arrayToList(String[] __strings)
	{
		List<String> __result = new ArrayList<String>();
		
		for (int i = 0; i < __strings.length; i++)
			__result.add(__strings[i]);
			
		return __result;
	}

	
	public static List<String> parse(String string, char split)
	{
		if (!isArray(string))
			return null;
		
		char[] __brackets = null;
		if (string.charAt(0) == '[')
			__brackets = "[]".toCharArray();
		else
			__brackets = "{}".toCharArray();

		return parse(string, split, __brackets);
	}

	public static boolean isURL(String string)
	{
		return contains(Arrays.asList("http", "https", "ftp", "//"), string, false);
	}
	/*
	 * This parses a given string which is a array in literal string form, to a list of it by given div and bracket([], {}, ()) chars
	 */
	public static List<String> parse(String string, char div, char[] bracket)
	{
		if (bracket.length != 2)
			throw new IllegalArgumentException("");
		
		List<String> list = new ArrayList<String>();
		
		int prev = 0;
		for (int i = 0; i < string.length(); i++)
		{
			char c = string.charAt(i);
			
			//means we are at the end
			if (c == bracket[1])
			{
				list.add(string.substring(prev, i).trim());
				continue;
			}
			
			if (c == bracket[0])
			{
				prev = i;
				continue;
			}
			
			if (c == div)
			{
				list.add(string.substring(prev + 1, i).trim());
				prev = i + 1;
			}
		}
		
		return list;
	}
}
