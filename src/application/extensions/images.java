package application.extensions;

import javafx.geometry.BoundingBox;
import javafx.scene.image.Image;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;

/*
	Just a random collection of useful and not so useful functions that 
	expand the functionality of javafx image class. 

	JavaFX has quite limited functionality on the image class, they cannot be cloned or anything
	
*/

public class images 
{
	//clones a javafx image (works for non animated images, not sure about apng's)

	public static Image clone(Image __image)
	{
		if (__image == null)
			throw new NullPointerException("Variable __image can't be null");

		//utilise the getBytes function to get the byte[] from javafx.image
		byte[] __stream = getBytes(__image);
		
		//create a new writable image with same width and height as the original
		WritableImage __temp = new WritableImage((int) __image.getWidth(), (int) __image.getHeight());
		
		//get the pixel writer from the empty image
		PixelWriter __writer = __temp.getPixelWriter();
		
		//copy the pixels from the source
		__writer.setPixels(0, 0, (int) __image.getWidth(), (int) __image.getHeight(), PixelFormat.getByteBgraInstance(), __stream, 0, (int) __image.getWidth() * 4);
		
		//whilst casting is not required, cast it anyways
		return (Image) __temp;
	}
	
	public static byte[] getBytes(Image __image)
	{
		if (__image == null)
			throw new NullPointerException("Variable __image can't be null");
		
		PixelReader __reader = __image.getPixelReader();

		int w = (int) __image.getWidth();
		int h = (int) __image.getHeight();
	
		//create a buffer that holds all bytes (4 is because we use BGRA, a
		byte[] __buffer = new byte[w * h * 4];
		
		__reader.getPixels(0, 0, w, h, PixelFormat.getByteBgraInstance(), __buffer, 0, w * 4);

		return __buffer;
	}
	
	
	//gets the javafx image dimensions or bounds
	public static BoundingBox dimensions(Image __image)
	{
		return new BoundingBox(0, 0, __image.getWidth(), __image.getHeight());
	}
}
