package application.gui.single;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import application.types.TPortal;
import application.types.TSettings;
import application.types.custom.gallery.TGallery;
import application.types.images.gif.devoder.GifDecoder;
import application.types.images.gif.devoder.GifDecoder.GifImage;
import application.types.images.gif.viewer.TGifViewer;
import application.types.images.image.data.TImage;
import application.types.images.image.data.TImage.Maps;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.WindowEvent;

/*
 * This controller will handle full images
 * 
 * TODO:
 * - There is an issue with full gif files, occasionally decoding them will throw java.lang.ArrayIndexOutOfBoundsException: 4096
 * - This can probably be circumvented by custom library/class that either uses LZW-decoding right way
 * - or show a image after image, with the FPS the gif is in.
 * 
 */

public class controller_single implements Initializable
{ 
	@FXML private ImageView iv_view;
	@FXML private AnchorPane ap_base;
	
	private TPortal __portal;
	private TGallery __gallery;
	private TImage __imagedata;
	private Double __width;
	private Double __height;
	private ExecutorService __executor;
	private File __file;
	private TGifViewer __viewer;

	public EventHandler<WindowEvent> exit()
	{

		return new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent event) {
				if (__viewer != null)
					__viewer.stop();
			}};
		
	}
	
	@Override
	public synchronized void initialize(URL arg0, ResourceBundle arg1) 
	{	
		__executor = Executors.newSingleThreadExecutor();
		
		// TODO Auto-generated method stub
		this.__portal = TPortal.instance();
		this.__gallery = __portal.getGallery();
		this.__imagedata = __gallery.getTileManager().getTile(__gallery.getTileManager().getFullID()).getData();
		
		this.__width = Double.valueOf(this.__imagedata.getProperty(Maps.DATA, "width"));
		this.__height = Double.valueOf(this.__imagedata.getProperty(Maps.DATA, "height"));
		
		__file = new File(TSettings.instance().getPath("cache") + "/derpibooru/" + this.__imagedata.getProperty(Maps.DATA, "id"));

		try {
			Files.copy(new URL(this.__imagedata.getProperty(Maps.LINKS, "image")).openStream(), __file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.__gallery.getTileManager().setFull(-1);
		
		
		if (this.__imagedata.getProperty(Maps.DATA, "original_format").equals("gif"))
		{
			
			try {
				GifImage __images = GifDecoder.read(new FileInputStream(__file));
				
				List<Image> __converted = new ArrayList<Image>();
				List<Integer> __delays = new ArrayList<Integer>();
				
				//make this multithreaded for better performance
				for (int i = 0; i < __images.getFrameCount(); i++)
				{
					__converted.add(SwingFXUtils.toFXImage(__images.getFrame(i), null));
					__delays.add(__images.getDelay(i));
				}	
				
				__viewer = new TGifViewer(__converted, __delays);

				this.__executor.submit(__viewer);
				this.ap_base.getChildren().add(__viewer.getContainer());
				this.iv_view.setVisible(false);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		else
		{
			this.iv_view.setFitWidth(this.__width);
			this.iv_view.setFitHeight(this.__height);
			try {
				this.iv_view.setImage(new Image(new FileInputStream(__file)));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}

}
