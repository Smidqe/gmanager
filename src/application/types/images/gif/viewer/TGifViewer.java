package application.types.images.gif.viewer;

import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

public class TGifViewer implements Runnable
{
	private int __delay, __position;
	private List<Image> __images;
	private List<Integer> __delays;
	private boolean __automatic;
	private ImageView __container;
	private boolean __stop;
	
	public TGifViewer(List<Image> images, List<Integer> delays) 
	{
		// TODO Auto-generated constructor stub
		this.__images = images;
		this.__delays = delays;
		this.__automatic = true;
		this.__container = new ImageView();
		
		this.__container.setFitHeight(images.get(0).getHeight());
		this.__container.setFitWidth(images.get(0).getWidth());
		this.__stop = false;
	}
	
	//can be used for buttons
	public void next(boolean move)
	{
		this.__container.setImage(__images.get(__position));
		
		if (move)
		{
			__position++;
		
		//reset the position
			if (__position > __images.size())
				__position = 0;
		}
	}
	
	//can be used for buttons
	public void previous(boolean move)
	{
		if (move && __position <= 0)
			__position = __images.size();
		
		this.__container.setImage(__images.get(__position));
		
		if (move)
			this.__position--;
	}
	
	public void control(KeyEvent event)
	{
		//perhaps some other control methods \\hmmm
		
		if (event.getCode() == KeyCode.RIGHT)
			next(true);
		else
			previous(true);
	}
	
	//for mouse handling
	public void control(MouseEvent event)
	{
		if (event.getClickCount() == 2)
		{
			this.__automatic = true;
			return;
		}

		switch (event.getButton())
		{
			case MIDDLE: break;
			case PRIMARY: next(true); break;
			case SECONDARY: previous(true); break;
			default:
				break;
		}
	}
	
	public ImageView getContainer()
	{
		return this.__container;
	}
	
	public int getDelay()
	{
		return this.__delay;
	}
	
	public List<Image> getImages()
	{
		return this.__images;
	}
	
	public void stop()
	{
		this.__stop = true;
	}
	
	@Override
	public void run() 
	{
		while (!this.__stop)
		{
			if (this.__automatic)
			{
				try {
			
					next(false);
					Thread.sleep(__delays.get(__position) * 10);
				
					if (__position >= __images.size() - 1)
						__position = 0;
					else
						__position++;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				continue;
			}
		}
	}
	
}
