package application.types.images.data.builder;

import java.util.concurrent.Callable;

import org.ini4j.Profile.Section;

import application.types.images.data.TImageInfo;

public class TImageInfoBuilder implements Callable<TImageInfo>
{
	private Section __section;
	
	public TImageInfoBuilder(Section section) 
	{
		this.__section = section;
		// TODO Auto-generated constructor stub
	}

	public void build(TImageInfo data)
	{
		for (String key : __section.keySet())
			data.getMap().put(key, __section.get(key));
	}
	
	@Override
	public TImageInfo call() throws Exception {
		TImageInfo __info = new TImageInfo();
		
		build(__info);
		
		return __info;
	}
}
