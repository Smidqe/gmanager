package application.types.images.data;

import java.util.Map;
import java.util.WeakHashMap;

//this will hold the necessary data for the images as to what to use for certain values
public class TImageInfo 
{
	private Map<String, String> __values;
	
	public TImageInfo() 
	{
		this.__values = new WeakHashMap<String, String>();
	}
	
	public Map<String, String> getMap()
	{
		return this.__values;
	}

}
