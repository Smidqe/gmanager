package application.types.images.image.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import application.extensions.arrays;
import application.extensions.strings;
import application.types.images.image.data.TImage;
import application.types.images.image.data.TImage.Maps;
import parser.TSection.TSection;

//rather simple building method
public class TImageBuilder implements Callable<TImage>
{
	private TSection<String, String> __data;
	private List<String> __skip;
	
	public TImageBuilder(TSection<String, String> data) 
	{
		this.__data = data;
		this.__skip = new ArrayList<String>();
	}
	
	public void setSkips(List<String> skip)
	{
		this.__skip = skip;
	}
	
	private void build(TImage image, TSection<String, String> data)
	{
		//add the data from the single values
		for (String key : data.getValues().keySet())
		{
			if (arrays.exists(__skip, key))
				continue;
			
			image.setProperty(Maps.DATA, key, data.getValues().get(key));
		}
		
		//for every subsections use recursive build
		for (String key : data.getSubsections().keySet())
		{
			//skip the subsections if they exist in __skip
			if (arrays.exists(__skip, key))
				continue;
				
			build(image, data.getSubsections().get(key));
		}
	}
	
	private void links(TImage image)
	{
		Map<String, String> __map = image.getMap(Maps.DATA);
		List<String> __list = Arrays.asList("https", "http", "//");
		List<String> __remove = new ArrayList<String>();
		
		//go through every value
		for (String key : __map.keySet())
		{
			//if it doesn't contain any indication for links, just skip them
			if (!strings.contains(__list, __map.get(key), false))
				continue;
			
			//check if we already have a protocol in them
			if (strings.contains(__list.subList(0, 1), __map.get(key), false))
				continue;
			
			//create a string with a https prefix, TODO: Utilise TSettings to get the proper protocol if the site
			//doesn't serve over https
			String fixed = "https:" + __map.get(key);
			
			//add it to the links map
			image.setProperty(Maps.LINKS, key, fixed);
			
			//add the moved key to a removal list
			__remove.add(key);
		}
		
		//remove the keys that were moved to links
		for (String key : __remove)
			image.getMap(Maps.DATA).remove(key);
	}
	
	@Override
	public TImage call() throws Exception {
		TImage __image = new TImage();
		
		build(__image, __data);
		links(__image);
		
		return __image;
	}

	public List<String> getSkips() {
		return this.__skip;
	}

}
