package application.types.custom.gallery.cache;

import java.io.IOException;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

import application.types.TSettings;
import application.types.factories.FThreadFactory;


/*
	TODO:
		- Once we have search done, remove the singleton property from this class
		and turn it into a normal object, also add utilise __path when creating it
		or perhaps change the name for it to identify the sub caches as multiple
		cache managers can work at the same time but not on same folder. Perhaps
		use a single cachemanager for a single site, so every site will have their own
		manager, then we can access the images directly if searching, instead of creating
		more memory usage
		- 

 */

public class TCacheManager implements Runnable
{
	public enum Status {IDLE, RUNNING, ERROR};
	
	private static TCacheManager __self = new TCacheManager();
	private boolean __stop;
	
	private BlockingDeque<TCacheAction> __queue;
	private Map<String, TCacheAction> __managed;
	private String __path;
	
	//eventually have multiple folders (for full images and thumbs)
	@SuppressWarnings("unused")
	private Map<String, String> __folders;
	
	//the executor for IO handlers.
	private ExecutorService __executor;
	private TCacheAction __job;
	private Status __status;
	
	private TCacheManager() 
	{
		this.__stop = false;
		
		this.__executor = Executors.newCachedThreadPool(new FThreadFactory("TCacheManager", "Subthreads", true));
		this.__queue = new LinkedBlockingDeque<TCacheAction>();
		this.__path = TSettings.instance().getPath("cache");
		this.__managed = new ConcurrentHashMap<String, TCacheAction>();
		
		this.__status = Status.IDLE;
	}

	public void add(TCacheAction action)
	{
		if (action == null)
			throw new NullPointerException("Variable is null, can't add a null object to queue");

		this.__queue.add(action);
	}
	
	public TCacheAction get(String id) throws InterruptedException
	{
		if (__managed == null)
			throw new NullPointerException("__managed list is not initialized");
		
		if (__managed.size() == 0)
			throw new ArrayIndexOutOfBoundsException("__managed's size is 0");

		TCacheAction __current = null;
		
		for (String _tid : this.__managed.keySet())
			if (_tid.equals(id))
			{
				__current = this.__managed.get(id);
				break;
			}
		
		return __current;
	}
	
	public boolean exists(String id) throws InterruptedException
	{
		if (__managed.size() == 0)
			return false;
		
		return (get(id) != null);
	}
	
	public boolean remove(String id) throws InterruptedException 
	{
		if (!this.exists(id))
			return true;
		
		this.__managed.remove(id);
		
		return exists(id);
	}
	
	public static TCacheManager instance()
	{
		return __self;
	}

	public Status getStatus()
	{
		return this.__status;
	}
	
	@Override
	public void run() 
	{
		while (!this.__stop)
		{
			this.__status = Status.IDLE;

			try {
				__job = this.__queue.take();
	
				if (exists(__job.getID()))
					continue;
				
				//means that we have gotten a stop call.
				if (__job.getID().equals("-1"))
					continue;
					
				this.__status = Status.RUNNING;
				
				__job.setFolder(__path);
				
				__executor.submit(__job);
				__managed.put(__job.getID(), __job);
				
				//what else to do here?
				//System.out.println("__managed.size: " + this.__managed.size());
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		System.out.println("TCacheManager shutting down");
	}

	public void stop() throws InterruptedException, IOException 
	{
		this.__stop = true;
		this.__queue.put(new TCacheAction("-1"));

		System.out.println("Deleting temp files");
		
		for (String action : __managed.keySet())
			__managed.get(action).stop();
		
		this.__executor.shutdown();
	}

	public void clear() {
		for (String action : __managed.keySet())
			__managed.get(action).stop();
		
		this.__managed.clear();
	}


}
