package application.types.custom.gallery.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;

import application.extensions.images;
import application.types.TSettings;
import javafx.scene.image.Image;

/*
 	TODO:
 		Refactor this, there are too many variables
 		
 */

public class TCacheAction implements Runnable
{
	public enum Status {IDLE, RUNNING, ERROR}
	public enum Action {READ, WRITE, STOP, SLEEP}
	
	private Image __image;
	private URL __url;
	private boolean __stop;
	private boolean __saved;
	private Action __action;
	private Status __status;
	private String __id;
	private String __folder;
	
	//rest of the variables will be handled in TCacheManager
	public TCacheAction(String id) 
	{
		this.__id = id;
	}
	
	public TCacheAction(String id, Image image) 
	{
		this.__image = image;
		this.__id = id;
		this.__folder = TSettings.instance().getPath("cache");
		this.__action = Action.SLEEP;
	}

	public TCacheAction(String id, URL url) 
	{
		this.__id = id;
		this.__folder = TSettings.instance().getPath("cache");
		this.__action = Action.SLEEP;
		this.__url = url;
	}
	
	//mainly used for reading to get the image
	public Image getImage()
	{
		System.out.println("this.__saved: " + this.__saved);
		
		if (!this.__saved)
			return null;
		
		if (this.__image == null)
			try {
				read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return this.__image;
	}

	public void setURL(URL url)
	{
		this.__url = url;
	}
	
	public String getID()
	{
		return this.__id;
	}
	
	public void setID(String id)
	{
		this.__id = id;
	}
	
	public void setFolder(String folder)
	{
		this.__folder = folder;
	}
	
	public void setAction(Action action)
	{
		this.__action = action;
	}
	
	public Action getAction()
	{
		return this.__action;
	}
	
	public Status getStatus()
	{
		return this.__status;
	}
	
	public void setImage(Image __img)
	{
		//prevent losing the image if it suddenly offloads from the tile, by cloning it
		this.__image = images.clone(__img); 
	}
	
	public void read() throws IOException, InterruptedException
	{
		//if the file doesn't exist, exit
		if (!Files.exists(Paths.get(__folder, __id), new LinkOption[] {LinkOption.NOFOLLOW_LINKS}))
			return;
		
		//create the file
		File __file = Paths.get(__folder, __id).toFile();
		
		while (!__file.canRead())
			Thread.sleep(1);
		
		//load the image using fileinputstream and use that stream to create javafx image
		InputStream __stream = new FileInputStream(__file);
		this.__image = new Image(__stream);
		
		//close the stream to release the file
		__stream.close();
	}
	
	public void write() throws InterruptedException, IOException
	{
		//rewrite the file if it already exists
		if (Files.exists(Paths.get(__folder, __id), new LinkOption[] {LinkOption.NOFOLLOW_LINKS}))
			Files.delete(Paths.get(__folder, __id));
		
		//create the file
		File __file = null;
		try {
			__file = Files.createFile(Paths.get(__folder, __id)).toFile();
		} catch (IOException e1) {
			System.out.println("Error occurred: Creating file");
			
			e1.printStackTrace();
			this.__status = Status.ERROR;
			return;
		}

		//check if we failed to create a file
		if (__file == null)
		{
			System.out.println("File is null");
			
			this.__status = Status.ERROR;
			return;
		}
		
		//waits until the file is writable
		while (!__file.canWrite())
			Thread.sleep(1);
		
		//grab the image from the url, this way we can ensure that we have all the necessary data.
		try
		{
			ReadableByteChannel __channel = Channels.newChannel(this.__url.openStream());
			FileOutputStream __out = new FileOutputStream(__file);

			__out.getChannel().transferFrom(__channel, 0, Long.MAX_VALUE);
			__out.close();
			
			this.__saved = true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Saved: " + this.__saved);
	}
	
	public void stop()
	{
		this.__stop = true;
		this.__action = Action.STOP;
	}

	@Override
	public void run() 
	{
//		System.out.println("TCacheAction: " + this.__id + " started.");
//		System.out.println("Preliminary action: " + this.__action);
		while (!this.__stop)
		{
			this.__status = Status.IDLE;
			
			try {
				//wait until we have been given a task
				while (this.__action == Action.SLEEP)
					Thread.sleep(1);
				
				this.__status = Status.RUNNING;
				
				switch (this.__action) {
				case READ:
				{
					read();
					break;
				}
				
				case WRITE:
				{
					System.out.println("Writing");
					
					write();
					break;
				}
				case STOP:
				{

					Files.deleteIfExists(Paths.get(this.__folder, this.__id));
					break;
				}
				default:
					break;
				}
			} catch (InterruptedException | IOException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			this.__action = Action.SLEEP; //set the action to null
		}
		
		//System.out.println("TCacheAction: Shutting down");
	}

}
