package application.types.custom.gallery;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import application.extensions.strings;
import application.types.TGrabber;
import application.types.TGrabber.Status;
import application.types.TPortal;
import application.types.TSettings;
import application.types.factories.FThreadFactory;
import application.types.sites.TSite;
import application.types.sites.search.TSearch;
import application.types.sites.search.query.TSearchQuery;
import application.types.custom.gallery.cache.TCacheManager;
import application.types.custom.gallery.tiles.TTileManager;
import application.types.custom.gallery.viewport.TViewport;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.TilePane;


/*
	TODO:
		- Add more methods
 */

public class TGallery implements Runnable
{
	public enum Count {IMAGES, PAGES};
	public enum Action {REFRESHER, GRABBER, SHUTDOWN};
	
	//JavaFX panes
	private ScrollPane __container;
	private TilePane __tiles;

	//custom made classes and other variables
	private TTileManager __manager;
	private TViewport __viewport;
	private TGrabber __grabber;
	private TCacheManager __cache;
	private TSite __site;
	//private TSearch __search;
	private TSettings __settings;
	private int __current_page = 1;
	private boolean __searching = false;
	private TPortal __portal;
	
	//private boolean __continuous; // for the future if user wants paged or continuous scrolling. also causes
	
	private ExecutorService __threads;
	
	
	public TGallery(TilePane tiles, ScrollPane container) throws MalformedURLException, InterruptedException 
	{
		//initialize some other necessary variables
		this.__cache = TCacheManager.instance();
		this.__tiles = tiles;
		this.__container = container;
		this.__portal = TPortal.instance();
		this.__viewport = new TViewport(this.__tiles, this.__container);
		this.__manager = new TTileManager(this.__viewport, this.__tiles);
		this.__grabber = new TGrabber(this.__manager); 
		
		
		//create a threadpool for the subthreads
		this.__threads = Executors.newCachedThreadPool(new FThreadFactory("TGallery", "Subthreads", true));

		//This is still for testing, once there is something to choose the site
		//the only site will be derpibooru
		
		this.__settings = TSettings.instance();
		this.__site = this.__settings.getSite("SITE_DERPIBOORU");
		//this.__search = new TSearch(this.__site.getURL("search"), this.__site.getVariables().get("pagePrefix"));
		
		this.__settings.setCurrentSite(this.__site);
		
		this.__grabber.add(this.__site.getURL("images", __current_page));
		
		//add a listener for checking if we have reached the bottom of the scroll
		__container.vvalueProperty().addListener(createScrollListener());
		
		//Add the classes to executor
		this.__threads.submit(this.__grabber);
		this.__threads.submit(this.__manager);
		this.__threads.submit(this.__cache);
		this.__portal.setGallery(this);
	}

	
	
	private ChangeListener<Number> createScrollListener()
	{
		return new ChangeListener<Number>(){

			@Override
			public synchronized void changed(ObservableValue<? extends Number> arg0, Number arg1, Number valueNew) 
			{
				try {					
					if ((valueNew.doubleValue() == 1.0) && (__grabber.getStatus() == Status.IDLE))
						__grabber.add(__site.getURL(__searching ? "search" : "images", ++__current_page));

				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}		
			
		};
	}
	
	public TViewport getViewport()
	{
		return this.__viewport;
	}
	
	public TSite getSite()
	{
		return this.__site;
	}

	public synchronized TilePane getTilePane()
	{
		return this.__tiles;
	}
	
	public ScrollPane getScrollPane()
	{
		return this.__container;
	}
	
	public TTileManager getTileManager()
	{
		return this.__manager;
	}
	
	//will eventually get the amount of pictures/pages loaded depending on argument id
	public int getAmount(Count id)
	{
		switch (id)
		{
			case PAGES: break; //return this.__manager.getPages()
			case IMAGES: return this.__manager.amount();

		}
		
		return -1;
	}

	public void stop() throws Exception 
	{
		__grabber.stop();
		__cache.stop();
		__manager.stop();

		__threads.shutdown();
		
	}



	public void search(String __string) 
	{
		this.__searching = true;
		
		//create the search query
		TSearchQuery __query = new TSearchQuery(strings.arrayToList(__string.split(",")));
		try {
			System.out.println(__query.call());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//check if we have anything loaded (pretty much always unless search is empty)
		//and clear/remove it all
		
		
		
//		if (!this.__settings.getBoolean("keepCache"))
//			this.__cache.clear();
//		
//		if (this.__manager.amount() > 0)
//			this.__manager.clear();
		
		
		
		//set the new url to grabber (might need a rework, since I want to move the code handling here or other places)
		//probably get the data from it and pass it to parsing
		
		
		//this.__grabber.add(this.__search.query());
		//load the data according to the json

	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
