package application.types;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.ini4j.Ini;
import org.ini4j.Profile.Section;

import application.types.images.data.TImageInfo;
import application.types.images.data.builder.TImageInfoBuilder;
import application.types.sites.TSite;
import application.types.sites.builder.TSiteBuilder;

public class TSettings 
{
	enum Maps {SETTINGS, PATHS, SITES, FILES};
	
	private Map<String, String> __settings; //holds all global variables
	private Map<String, String> __paths; //holds all paths
	private Map<String, TSite> __sites; //holds all sites
	private Map<String, Ini> __files;
	private Map<String, TImageInfo> __images;
	
	private TSite __site = null;
	
	//keep this as a singleton
	private static TSettings self = new TSettings();
	
	public synchronized static TSettings instance()
	{
		return self;
	}
	
	protected TSettings()
	{
		this.__settings = new HashMap<String, String>();
		this.__paths = new HashMap<String, String>();
		this.__sites = new HashMap<String, TSite>();
		this.__files = new HashMap<String, Ini>();
		this.__images = new HashMap<String, TImageInfo>();
		
		this.load();
	}

	public void setCurrentSite(TSite site)
	{
		this.__site = site;
	}
	
	public TSite getCurrentSite()
	{
		return this.__site;
	}
	
	//i'm not happy about this, but for a first version this is alright
	private void load()
	{
		File __folder = new File("src/application/data/resources/config/");
		File[] __files = __folder.listFiles();
		
		if (__files.length == 0)
			return;
		
		Ini __iniFile = null;
		for (int i = 0; i < __files.length; i++)
		{
			try {
				__iniFile = new Ini(__files[i]);
				
				String __filename = __files[i].getName().substring(0, __files[i].getName().indexOf('.'));
				
				if (!this.__files.containsKey(__filename))
					this.__files.put(__filename, __iniFile);

				switch (__filename)
				{
					case "settings":
					{
						for (String key : __iniFile.keySet())
						{
							Section __section = __iniFile.get(key);
							
							for (String __value : __section.keySet())
								switch(key)
								{
								case "PATHS": this.__paths.put(__value, __section.get(__value)); break;
								case "SETTINGS": this.__settings.put(__value, __section.get(__value)); break;
								case "FILES": this.__files.put(__value, new Ini(new File(__section.get(__value)))); break;
								}
						}
							
						break;
					}
					
					case "sites":
					case "images":
					{
						
						for (String key : __iniFile.keySet())
						{
							System.out.println(key);
							if (__filename.equals("sites"))
								this.__sites.put(key, new TSiteBuilder(__iniFile.get(key)).call());
							else
								this.__images.put(key, new TImageInfoBuilder(__iniFile.get(key)).call());
						}
						break;
					}

				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void save(Maps map)
	{
		switch(map)
		{
		case FILES:
			break;
		case PATHS:
			break;
		case SETTINGS:
			break;
		case SITES:
			break;
		default:
			break;
		
		}
	}
	
	//todo, add a checks that the value is actually instance of class, so we don't cast it wrongly
	public void change(Maps map, String key, Object value, boolean save)
	{
		switch(map)
		{
			case FILES: 
			{
				if (!(value instanceof Ini))
					throw new IllegalArgumentException("Value is not a Ini, actual: " + value.getClass().getName());
				
				this.__files.put(key, (Ini) value); 
				break;
			}
			
			case PATHS: 
			{
				if (!(value instanceof String))
					throw new IllegalArgumentException("Value is not a String, actual: " + value.getClass().getName());
				this.__paths.put(key, (String) value); 
				break;
			}
			
			case SETTINGS:
			{
				if (!(value instanceof String))
					throw new IllegalArgumentException("Value is not a String, actual: " + value.getClass().getName());
				
				this.__settings.put(key, (String) value); 
				break;
			}
			
			case SITES:
			{
				if (!(value instanceof TSite))
					throw new IllegalArgumentException("Value is not a TSite, actual: " + value.getClass().getName());
				
				this.__sites.put(key, (TSite) value);
				break;
			}
			
			default:
				break;
		
		}
		
		//save the file
		if (save)
			save(map);
	}

	public Map<String, Ini> getFiles()
	{
		return this.__files;
	}
	
	public Map<String, String> getSettings()
	{
		return this.__settings;
	}
	
	public Map<String, TImageInfo> getImageInfo()
	{
		return this.__images;
	}
	
	public Map<String, TSite> getSites()
	{
		return this.__sites;
	}
	
	public synchronized String getString(String key)
	{
		return this.__settings.get(key);
	}
	
	public synchronized boolean getBoolean(String key)
	{
		return Boolean.parseBoolean(this.__settings.get(key));
	}
	
	public synchronized int getInteger(String key)
	{
		return Integer.parseInt(this.__settings.get(key));
	}
	
	public synchronized Map<String, String> getPaths()
	{
		return this.__paths;
	}
	
	public synchronized String getPath(String key)
	{
		return this.__paths.get(key);
	}
	
	public synchronized TSite getSite(String key)
	{
		return this.__sites.get(key);
	}
	
	
	//add rest of the functions later.
	/*
	 	load
	 */
}
