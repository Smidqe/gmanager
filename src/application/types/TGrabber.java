package application.types;

import java.net.MalformedURLException;

/*
	TODO:
		- Rewrite this
			- We could change the deque so that it accepts URL's instead of Action enum
			- This way we can give it a url without setting a url first
			- And it would generalize it
			- Theoretically it could work with files aswell
			- But for our purposes only loading JSON data is fine

 */

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;

import application.extensions.connections;
import application.extensions.strings;
import application.types.custom.gallery.tiles.TTileManager;
import application.types.custom.gallery.tiles.tile.TTile;
import application.types.factories.FThreadFactory;
import application.types.images.image.builder.TImageBuilder;
import application.types.images.image.data.TImage;
import application.types.interfaces.IWebCodes;
import application.types.interfaces.IWebCodes.Codes;
import javafx.application.Platform;
import javafx.scene.image.ImageView;
import parser.TParser;
import parser.TSection.TSection;


public class TGrabber implements Runnable
{
	public enum Status {IDLE, RUNNING, ERROR};
	
	private TTileManager __tiles;
	private boolean stop;
	private BlockingDeque<URL> __deque;
	private Status __status;

	public TGrabber(TTileManager manager) 
	{
		this.__tiles = manager;
		this.__deque = new LinkedBlockingDeque<URL>();
		
		this.__status = Status.IDLE;
	}

	public void stop() throws MalformedURLException, InterruptedException
	{
		this.stop = true;
		Thread.currentThread().interrupt();
	}
	
	public Status getStatus()
	{
		return this.__status;
	}
	
	public void add(URL url) throws InterruptedException
	{
		this.__deque.put(url);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		ExecutorService __executor = Executors.newCachedThreadPool(new FThreadFactory("TGrabber", "__executor", true));

		URL __url = null;
		while (!this.stop)
		{
			System.out.println(this.__status);
			
			
			try {
				__url = this.__deque.take();
			} catch (InterruptedException e1) {
				if (!this.stop)
					e1.printStackTrace();
			}
			
			
			
			System.out.println("TGrabber - Starting loading, URL: " + __url);
			
			this.__status = Status.RUNNING;
			
			if (__url == null)
				break;
			
			try {
				String response = connections.ping(__url);
				
				if (IWebCodes.inRange(response, Codes.SUCCESS))
				{
					TParser __parser = new TParser(__url);

					__parser.parse();
					
					/*
					 * I need to eventually think at what level the images will be, is it always the second one or first one
					 */
					
					TSection<String, String> __data = __parser.get().getSubsections().get("images");

					List<Future<TImage>> __futures = new ArrayList<Future<TImage>>();
					List<TTile> __containers =  new ArrayList<TTile>();
					
					for (String sub : __data.getSubsections().keySet())
					{
						TImageBuilder __builder = new TImageBuilder(__data.getSubsections().get(sub));
						__builder.setSkips(strings.parse(TSettings.instance().getSite("SITE_DERPIBOORU").getVariables().get("skip"), ','));
						
						System.out.println(__builder.getSkips());
						__futures.add(__executor.submit(__builder));
					}
					
					for (Future<TImage> f : __futures)
						__containers.add(new TTile(f.get(), new ImageView()));
					
					//has to happen on JavaFX thread, otherwise there will be problems
					Platform.runLater(new Runnable() {
						
						@Override
						public void run() {
							try {
								__tiles.add(__containers);
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
					
				}
				else
				{
					System.out.println("TGrabber - Ping resulted: " + response + ", grabber will not run.");
				}
				
				System.out.println(this.__status);
				this.__status = Status.IDLE;
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				break;
			}
			
			System.out.println("Ending");
		}
		
		__executor.shutdown();
		System.out.println("TGrabber - Shutting down");
	}

	public void setStatus(Status status) 
	{
		this.__status = status;
	}
}