package application.types.interfaces;

import java.util.List;

public interface Query 
{
	public void create(List<String> parameters);
	
	public Query get();
}
