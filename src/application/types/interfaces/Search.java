package application.types.interfaces;

public interface Search {
	public enum Direction {NEXT, PREVIOUS};
	
	public boolean search();
	public void save();
	public void getQuery(int index);
	public boolean validate();
	public void control(Direction direction);
}
