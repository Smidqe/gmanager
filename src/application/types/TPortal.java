package application.types;

import application.types.custom.gallery.TGallery;

//will be a singleton and will hold the current gallery, depending on which tab/page we are on.

public class TPortal
{
	private static TPortal __instance = new TPortal();
	
	private TGallery __gallery;
	
	
	protected TPortal() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public void setGallery(TGallery gallery)
	{
		this.__gallery = gallery;
	}
	
	public TGallery getGallery()
	{
		return this.__gallery;
	}
	
	public static TPortal instance()
	{
		return __instance;
	}
}
