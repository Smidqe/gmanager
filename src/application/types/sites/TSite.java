package application.types.sites;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import application.types.sites.filters.filter.TFilter;

public class TSite 
{
	public enum MAPS {VARIABLE, URLS};
	
	private String name;

	private List<String> __skippables;

	private Map<String, String> __variables;
	private Map<String, URL> __urls;
	private Map<String, TFilter> __filters;
	
	public TSite()
	{
		this.__skippables = new ArrayList<String>();
		this.__urls = new HashMap<String, URL>();
		this.__variables = new HashMap<String, String>();
		this.__filters = new HashMap<String, TFilter>();
	}

	public Map<String, TFilter> getFilters()
	{
		return this.__filters;
	}
	
	public void putURL(String name, URL url) throws MalformedURLException
	{
		this.__urls.put(name, url);
	}

	public void addSkippable(String ID)
	{
		this.__skippables.add(ID);
	}

	public boolean isSearcheable()
	{
		return Boolean.valueOf(this.__variables.get("search"));
	}

	public List<String> getSkippables()
	{
		return this.__skippables;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public Map<String, URL> getURLs()
	{
		return this.__urls;
	}
	
	public URL getURL(String name)
	{
		return getURL(name, 0);
	}
	
	public URL getURL(String name, int page)
	{
		if (page == 0)
			return (URL) this.__urls.get(name);
		
		try {
			return new URL(__urls.get(name).toString() + __variables.get("pageprefix") + String.valueOf(page));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Map<String, String> getVariables() {
		// TODO Auto-generated method stub
		return this.__variables;
	}
}
