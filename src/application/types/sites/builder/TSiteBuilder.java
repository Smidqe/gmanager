package application.types.sites.builder;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.Callable;

import org.ini4j.Profile.Section;

import application.extensions.strings;
import application.types.sites.TSite;

/*
 * This class will build the sites from the ini files
 */
public class TSiteBuilder implements Callable<TSite>
{
	Section __data = null;

	public TSiteBuilder(Section section) 
	{
		this.__data = section;
	}

	public TSite build() throws MalformedURLException
	{
		TSite __site = new TSite();
		
		for (String key : this.__data.keySet())
		{
			System.out.println(key + ": " + this.__data.get(key));
			
			if (!key.equals("protocol"))
				if (strings.contains(Arrays.asList("https", "http"), this.__data.get(key), false) && strings.isURL(this.__data.get(key)))
				{
					__site.putURL(key, new URL(this.__data.get(key)));
					continue;
				}
			
			//While we have arrays in it don't add them as arrays
			
			__site.getVariables().put(key, this.__data.get(key));
		}

		return __site;
	}

	@Override
	public TSite call() throws Exception 
	{
		if (this.__data == null)
			throw new NullPointerException("");
		
		return build();
	}
}
