package application.types.sites.search.query;

import java.util.List;
import java.util.concurrent.Callable;

import application.types.TSettings;
import application.types.sites.TSite;

/*
	This will handle the search querys, possibly have multiple
	build commands to help with other sites.
	 
 */

public class TSearchQuery implements Callable<String>
{
	private List<String> __parameters;
	private String __query;

	public TSearchQuery(List<String> __parameters) 
	{
		this.__parameters = __parameters;
		// TODO Auto-generated constructor stub
	}

	public String build()
	{
		//utilise TSettings.currentSite().specials();
		TSite __site = TSettings.instance().getCurrentSite();
		
		if (!__site.isSearcheable())
			return null;
		
		String __replace = __site.getVariables().get("replaceSpace");
		
		StringBuilder __builder = new StringBuilder();
		String __string = null;
		
		//add user key before anything
		//__builder.append("key=" + users.getCurrentUser().getKey()
		
		//add the prefix for query if it exists
		//__builder.append(__site.getVariable("queryPrefix")
		
		
		//add the search objects
		for (int i = 0; i < __parameters.size(); i++)
		{
			__string = __parameters.get(i);
			__builder.append(__string.trim().replaceAll(" ", __replace));
			
			if (i != __parameters.size() - 1)
				__builder.append(',');
		}
		
		return __builder.toString();
	}

	public String get() {
		// TODO Auto-generated method stub
		return this.__query;
	}

	@Override
	public String call() throws Exception {
		// TODO Auto-generated method stub
		return build();
	}

	public boolean validate() {
		// TODO Auto-generated method stub
		return false;
	}
}
