package application.types.sites.search;

import java.net.MalformedURLException;
import java.net.URL;

import application.types.sites.search.query.TSearchQuery;

/*
 * Eventually I want to keep a track of what searches have been used, probably saved to
 * temp file.
 */

@SuppressWarnings("unused")
public class TSearch 
{
	private URL __url;
	private TSearchQuery __query;
	private String pagePrefix;
	
	public TSearch(URL url, String pagePrefix) 
	{
		this.__url = url;
		this.pagePrefix = pagePrefix;
	}
	
	public void setQuery(TSearchQuery query)
	{
		this.__query = query;		
	}
	
	public TSearchQuery getQuery()
	{
		return this.__query;
	}
	
	public void search(int page) throws MalformedURLException
	{
		if (this.__query == null)
			return;
		
		if (this.__query.equals(""))
			return;
		
		if (!this.__query.validate())
			return;
		
		URL __fullURL = new URL(this.__url.toString() + this.__query.get() + pagePrefix + page);
	}
	
	public URL getURL()
	{
		return this.__url;
	}
}
